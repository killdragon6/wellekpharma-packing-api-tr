package main

import (
	"main/handler"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// tr_before_delivery_checklist
	e.GET("/gettr_before_delivery_checklistall", handler.Gettr_before_delivery_checklistall)
	e.POST("/gettr_before_delivery_checklistbyid", handler.Gettr_before_delivery_checklistbyid)
	e.POST("/addtr_before_delivery_checklist", handler.Addtr_before_delivery_checklist)
	e.POST("/updatetr_before_delivery_checklist", handler.Updatetr_before_delivery_checklist)
	e.GET("/getsa_work_sa_ok", handler.Getsa_work_sa_ok)

	// tr_product_list
	e.GET("/gettr_product_listtall", handler.Gettr_product_listtall)
	e.POST("/gettr_product_listtbyid", handler.Gettr_product_listtbyid)
	e.POST("/gettr_product_listbyidnew", handler.Gettr_product_listbyidnew)
	e.POST("/addtr_product_listt", handler.Addtr_product_listt)
	e.POST("/updatetr_product_listt", handler.Updatetr_product_listt)
	e.POST("/gettr_product_listtbysacustomercode", handler.Gettr_product_listtbysacustomercode)

	e.Logger.Fatal(e.Start(":7006"))
}
