package handler

import (
	"main/model"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

// Pending Checklis
func Gettr_before_delivery_checklistall(c echo.Context) error {
	res := model.Gettr_before_delivery_checklistall(c)
	return c.JSON(http.StatusOK, res)
}
func Gettr_before_delivery_checklistbyid(c echo.Context) error {
	res := model.Gettr_before_delivery_checklistbyid(c)
	return c.JSON(http.StatusOK, res)
}
func Addtr_before_delivery_checklist(c echo.Context) error {
	res := model.Addtr_before_delivery_checklist(c)
	return c.JSON(http.StatusOK, res)
}
func Updatetr_before_delivery_checklist(c echo.Context) error {
	res := model.Updatetr_before_delivery_checklist(c)
	return c.JSON(http.StatusOK, res)
}
func Gettr_product_listtall(c echo.Context) error {
	res := model.Gettr_product_listtall(c)
	return c.JSON(http.StatusOK, res)
}
func Gettr_product_listtbyid(c echo.Context) error {
	res := model.Gettr_product_listtbyid(c)
	return c.JSON(http.StatusOK, res)
}
func Gettr_product_listbyidnew(c echo.Context) error {
	res := model.Gettr_product_listbyidnew(c)
	return c.JSON(http.StatusOK, res)
}
func Addtr_product_listt(c echo.Context) error {
	res := model.Addtr_product_listt(c)
	return c.JSON(http.StatusOK, res)
}
func Updatetr_product_listt(c echo.Context) error {
	res := model.Updatetr_product_listt(c)
	return c.JSON(http.StatusOK, res)
}
func Getsa_work_sa_ok(c echo.Context) error {
	res := model.Getsa_work_sa_ok(c)
	return c.JSON(http.StatusOK, res)
}
func Gettr_product_listtbysacustomercode(c echo.Context) error {
	res := model.Gettr_product_listtbysacustomercode(c)
	return c.JSON(http.StatusOK, res)
}
