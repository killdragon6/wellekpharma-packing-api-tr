package model

import (
	"encoding/json"

	"github.com/labstack/echo"
)

// SA
func Gettr_product_listtall(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	// query := "SELECT * FROM sa_invoice_checklist where sa_status = 'sa_ok'"
	// query := "SELECT * FROM tr_product_list where tr_status = 'tr_wait' OR tr_status = 'copy' "
	query := "SELECT * FROM tr_product_list"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id string
		var trbillnumber string
		var trbillingtime string
		var linetr string
		var trcustomercode string
		var trcustomername string
		var pkemployee_name string
		var pkemployee_code string
		var count_boxs string
		var localtion string
		var startdate string
		var enddate string
		var tr_code string
		var driver_code string
		var carregistration string
		var note string
		var image string
		var latitude string
		var longitude string
		var tr_status string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		rows.Scan(
			&id,
			&trbillnumber,
			&trbillingtime,
			&linetr,
			&trcustomercode,
			&trcustomername,
			&pkemployee_name,
			&pkemployee_code,
			&count_boxs,
			&localtion,
			&startdate,
			&enddate,
			&tr_code,
			&driver_code,
			&carregistration,
			&note,
			&image,
			&latitude,
			&longitude,
			&tr_status,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["trbillnumber"] = trbillnumber
		elements["trbillingtime"] = trbillingtime
		elements["linetr"] = linetr
		elements["trcustomercode"] = trcustomercode
		elements["trcustomername"] = trcustomername
		elements["pkemployee_name"] = pkemployee_name
		elements["pkemployee_code"] = pkemployee_code
		elements["count_boxs"] = count_boxs
		elements["localtion"] = localtion
		elements["startdate"] = startdate
		elements["enddate"] = enddate
		elements["tr_code"] = tr_code
		elements["driver_code"] = driver_code
		elements["carregistration"] = carregistration
		elements["note"] = note
		elements["image"] = image
		elements["latitude"] = latitude
		elements["longitude"] = longitude
		elements["tr_status"] = tr_status
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Gettr_product_listbyidnew(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	// query := "SELECT * FROM sa_invoice_checklist where sa_status = 'sa_ok'"
	// rows, err := DB.Query("SELECT * FROM tr_product_list where tr_status = 'tr_wait' OR tr_status = 'copy'  AND id = ?", json_map["id"])
	rows, err := DB.Query("SELECT * FROM tr_product_list where id = ?", json_map["id"])

	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id string
		var trbillnumber string
		var trbillingtime string
		var linetr string
		var trcustomercode string
		var trcustomername string
		var pkemployee_name string
		var pkemployee_code string
		var count_boxs string
		var localtion string
		var startdate string
		var enddate string
		var tr_code string
		var driver_code string
		var carregistration string
		var note string
		var image string
		var latitude string
		var longitude string
		var tr_status string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		rows.Scan(
			&id,
			&trbillnumber,
			&trbillingtime,
			&linetr,
			&trcustomercode,
			&trcustomername,
			&pkemployee_name,
			&pkemployee_code,
			&count_boxs,
			&localtion,
			&startdate,
			&enddate,
			&tr_code,
			&driver_code,
			&carregistration,
			&note,
			&image,
			&latitude,
			&longitude,
			&tr_status,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["trbillnumber"] = trbillnumber
		elements["trbillingtime"] = trbillingtime
		elements["linetr"] = linetr
		elements["trcustomercode"] = trcustomercode
		elements["trcustomername"] = trcustomername
		elements["pkemployee_name"] = pkemployee_name
		elements["pkemployee_code"] = pkemployee_code
		elements["count_boxs"] = count_boxs
		elements["localtion"] = localtion
		elements["startdate"] = startdate
		elements["enddate"] = enddate
		elements["tr_code"] = tr_code
		elements["driver_code"] = driver_code
		elements["carregistration"] = carregistration
		elements["note"] = note
		elements["image"] = image
		elements["latitude"] = latitude
		elements["longitude"] = longitude
		elements["tr_status"] = tr_status
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Gettr_product_listtbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	// rows, err := DB.Query("SELECT * FROM sa_invoice_number_checklist sa_n INNER JOIN sa_invoice_checklist sa_i ON sa_n.id = sa_i.id_invoice_list AND sa_n.id = ?", json_map["id"])
	// rows, err := DB.Query("SELECT sa_n.invoice_number, sa_n.invoice_price ,sa_i.sabillingtime, sa_i.linesa, sa_i.sacustomercode, sa_i.sacustomername, sa_i.sabillnumber ,sa_i.employee_name, sa_i.employee_id, sa_i.localtion, sa_i.count_boxs, sa_i.pk_waiting_checklist_id ,sa_i.start_pack, sa_i.end_pack, sa_i.qc_name, sa_i.qc_id, sa_i.sa_status , sa_i.sa_note , sa_i.id_invoice_list FROM sa_invoice_number_checklist sa_n INNER JOIN sa_invoice_checklist sa_i ON sa_n.id = sa_i.id_invoice_list AND sa_n.id = ?;", json_map["id"])
	rows, err := DB.Query("SELECT sa_n.invoice_number, sa_n.invoice_price ,sa_i.sabillingtime, sa_i.linesa, sa_i.sacustomercode, sa_i.sacustomername, sa_i.sabillnumber ,sa_i.employee_name, sa_i.employee_id, sa_i.localtion, sa_i.count_boxs, sa_i.pk_waiting_checklist_id ,sa_i.start_pack, sa_i.end_pack, sa_i.qc_name, sa_i.qc_id, sa_i.sa_status , sa_i.sa_note , sa_i.id_invoice_list FROM sa_invoice_number_checklist sa_n INNER JOIN sa_invoice_checklist sa_i ON sa_n.id = sa_i.id_invoice_list AND sa_n.invoice_number = ?;", json_map["invoice_number"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var invoice_number string
		var invoice_price string
		var sabillingtime string
		var linesa string
		var sacustomercode string
		var sacustomername string
		var sabillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var sa_status string
		var sa_note string
		var id_invoice_list string
		rows.Scan(
			&invoice_number,
			&invoice_price,
			&sabillingtime,
			&linesa,
			&sacustomercode,
			&sacustomername,
			&sabillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&sa_status,
			&sa_note,
			&id_invoice_list,
		)
		elements := make(map[string]interface{})
		elements["invoice_number"] = invoice_number
		elements["invoice_price"] = invoice_price
		elements["sabillingtime"] = sabillingtime
		elements["linesa"] = linesa
		elements["sacustomercode"] = sacustomercode
		elements["sacustomername"] = sacustomername
		elements["sabillnumber"] = sabillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["sa_status"] = sa_status
		elements["sa_note"] = sa_note
		elements["id_invoice_list"] = id_invoice_list
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Addtr_product_listt(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	// rows, err := DB.Exec("INSERT INTO tr_product_list (`trbillnumber`, `driver_code`, `linetr`, `carregistration`, `date`, `branch`, `forklift_code`, `start_time`, `time_departure`, `first_job_time`, `final_job_time`, `return_time`, `finish_time`, `km_start_work`, `km_end_work`, `km_run_work`, `total_fuel_cost`, `total_liter`, `km_fill`, `total_expressway_fees`, `total_parking_fee`, `car_mirror`, `car_lights`, `brake_light`, `tire`, `wiper`, `key_lock`, `spare_tire`, `water_boiler`, `brake_pads`, `glass_washing_water`, `engine_oil`, `battery_pot_water`, `telephone`, `tr_note`, `number_jobs_sent`, `number_jobs_nosent`, `create_date`, `create_at`, `modify_date`, `modify_at`)   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["trbillnumber"], json_map["driver_code"], json_map["linetr"], json_map["carregistration"], json_map["date"], json_map["branch"], json_map["forklift_code"], json_map["start_time"], json_map["time_departure"], json_map["first_job_time"], json_map["final_job_time"], json_map["return_time"], json_map["finish_time"], json_map["km_start_work"], json_map["km_end_work"], json_map["km_run_work"], json_map["total_fuel_cost"], json_map["total_liter"], json_map["km_fill"], json_map["total_expressway_fees"], json_map["total_parking_fee"], json_map["car_mirror"], json_map["car_lights"], json_map["brake_light"], json_map["tire"], json_map["wiper"], json_map["key_lock"], json_map["spare_tire"], json_map["water_boiler"], json_map["brake_pads"], json_map["glass_washing_water"], json_map["engine_oil"], json_map["battery_pot_water"], json_map["telephone"], json_map["tr_note"], json_map["number_jobs_sent"], json_map["number_jobs_nosent"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])
	rows, err := DB.Exec("INSERT INTO tr_product_list (`trbillnumber`,`trbillingtime`,`linetr`,`trcustomercode`,`trcustomername`,`pkemployee_name`,`pkemployee_code`,`count_boxs`,`localtion`,`startdate`,`enddate`,`tr_code`,`driver_code`,`carregistration`,`note`,`image`,`latitude`,`longitude`,`tr_status`,`create_date`,`create_at`,`modify_date`,`modify_at`)   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["trbillnumber"], json_map["trbillingtime"], json_map["linetr"], json_map["trcustomercode"], json_map["trcustomername"], json_map["pkemployee_name"], json_map["pkemployee_code"], json_map["count_boxs"], json_map["localtion"], json_map["startdate"], json_map["enddate"], json_map["tr_code"], json_map["driver_code"], json_map["carregistration"], json_map["note"], json_map["image"], json_map["latitude"], json_map["longitude"], json_map["tr_status"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])

	if err != nil {
		panic(err)
	}
	lastID, err := rows.LastInsertId()
	println("The last inserted row id:", lastID)
	if err != nil {
		panic(err)
	}

	res := make(map[string]interface{})

	res["data"] = lastID
	return res["data"]
}
func Updatetr_product_listt(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	var stringupdate string

	if json_map["trbillnumber"] != nil {
		stringupdate += "trbillnumber = '" + json_map["trbillnumber"].(string) + "',"
	}
	if json_map["trbillingtime"] != nil {
		stringupdate += "trbillingtime = '" + json_map["trbillingtime"].(string) + "',"
	}
	if json_map["linetr"] != nil {
		stringupdate += "linetr = '" + json_map["linetr"].(string) + "',"
	}
	if json_map["trcustomercode"] != nil {
		stringupdate += "trcustomercode = '" + json_map["trcustomercode"].(string) + "',"
	}
	if json_map["trcustomername"] != nil {
		stringupdate += "trcustomername = '" + json_map["trcustomername"].(string) + "',"
	}
	if json_map["pkemployee_name"] != nil {
		stringupdate += "pkemployee_name = '" + json_map["pkemployee_name"].(string) + "',"
	}
	if json_map["pkemployee_code"] != nil {
		stringupdate += "pkemployee_code = '" + json_map["pkemployee_code"].(string) + "',"
	}
	if json_map["count_boxs"] != nil {
		stringupdate += "count_boxs = '" + json_map["count_boxs"].(string) + "',"
	}
	if json_map["localtion"] != nil {
		stringupdate += "localtion = '" + json_map["localtion"].(string) + "',"
	}
	if json_map["startdate"] != nil {
		stringupdate += "startdate = '" + json_map["startdate"].(string) + "',"
	}
	if json_map["enddate"] != nil {
		stringupdate += "enddate = '" + json_map["enddate"].(string) + "',"
	}
	if json_map["tr_code"] != nil {
		stringupdate += "tr_code = '" + json_map["tr_code"].(string) + "',"
	}
	if json_map["driver_code"] != nil {
		stringupdate += "driver_code = '" + json_map["driver_code"].(string) + "',"
	}
	if json_map["carregistration"] != nil {
		stringupdate += "carregistration = '" + json_map["carregistration"].(string) + "',"
	}
	if json_map["note"] != nil {
		stringupdate += "note = '" + json_map["note"].(string) + "',"
	}
	if json_map["image"] != nil {
		stringupdate += "image = '" + json_map["image"].(string) + "',"
	}
	if json_map["latitude"] != nil {
		stringupdate += "latitude = '" + json_map["latitude"].(string) + "',"
	}
	if json_map["longitude"] != nil {
		stringupdate += "longitude = '" + json_map["longitude"].(string) + "',"
	}
	if json_map["tr_status"] != nil {
		stringupdate += "tr_status = '" + json_map["tr_status"].(string) + "',"
	}
	if json_map["create_date"] != nil {
		stringupdate += "create_date = '" + json_map["create_date"].(string) + "',"
	}
	if json_map["create_at"] != nil {
		stringupdate += "create_at = '" + json_map["create_at"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}

	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE tr_product_list
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Exec(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	if err != nil {
		panic(err)
	}
	count, err := rows.RowsAffected()
	if err != nil {
		panic(err)
	}

	res := make(map[string]interface{})
	res["data"] = count
	return res["data"]
}
func Gettr_product_listtbysacustomercode(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	rows, err := DB.Query("SELECT * FROM `admin_customer` WHERE customer_id = ?", json_map["customer_id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var customer_id string
		var customer_name string
		var customer_address string
		var inline_sequence string
		var line string
		var phone_number_1 string
		var phone_number_2 string
		var phone_number_3 string
		var latitude string
		var longitude string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(&id,
			&customer_id,
			&customer_name,
			&customer_address,
			&inline_sequence,
			&line,
			&phone_number_1,
			&phone_number_2,
			&phone_number_3,
			&latitude,
			&longitude,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["latitude"] = latitude
		elements["longitude"] = longitude

		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
