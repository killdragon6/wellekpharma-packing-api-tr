package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"unicode/utf8"

	"strconv"

	"github.com/labstack/echo"
)

var DB *sql.DB

func Initialize() {
	db, err := sql.Open("mysql", "root:@/wellekpharma_packing_system")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("connectDB")
	}
	DB = db
}

// SA
func Gettr_before_delivery_checklistall(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "SELECT * FROM tr_before_delivery_checklist where 1"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var trbillnumber string
		var id_trbillnumber string
		var driver_code string
		var linetr string
		var carregistration string
		var date string
		var branch string
		var forklift_code string
		var start_time string
		var time_departure string
		var first_job_time string
		var final_job_time string
		var return_time string
		var finish_time string
		var km_start_work string
		var km_end_work string
		var km_run_work string
		var total_fuel_cost string
		var total_liter string
		var km_fill string
		var total_expressway_fees string
		var total_parking_fee string
		var car_mirror int
		var car_lights int
		var brake_light int
		var tire int
		var wiper int
		var key_lock int
		var spare_tire int
		var water_boiler int
		var brake_pads int
		var glass_washing_water int
		var engine_oil int
		var battery_pot_water int
		var telephone int
		var tr_note string
		var number_jobs_sent string
		var number_jobs_nosent string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&trbillnumber,
			&id_trbillnumber,
			&driver_code,
			&linetr,
			&carregistration,
			&date,
			&branch,
			&forklift_code,
			&start_time,
			&time_departure,
			&first_job_time,
			&final_job_time,
			&return_time,
			&finish_time,
			&km_start_work,
			&km_end_work,
			&km_run_work,
			&total_fuel_cost,
			&total_liter,
			&km_fill,
			&total_expressway_fees,
			&total_parking_fee,
			&car_mirror,
			&car_lights,
			&brake_light,
			&tire,
			&wiper,
			&key_lock,
			&spare_tire,
			&water_boiler,
			&brake_pads,
			&glass_washing_water,
			&engine_oil,
			&battery_pot_water,
			&telephone,
			&tr_note,
			&number_jobs_sent,
			&number_jobs_nosent,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["trbillnumber"] = trbillnumber
		elements["id_trbillnumber"] = id_trbillnumber
		elements["driver_code"] = driver_code
		elements["linetr"] = linetr
		elements["carregistration"] = carregistration
		elements["date"] = date
		elements["branch"] = branch
		elements["forklift_code"] = forklift_code
		elements["start_time"] = start_time
		elements["time_departure"] = time_departure
		elements["first_job_time"] = first_job_time
		elements["final_job_time"] = final_job_time
		elements["return_time"] = return_time
		elements["finish_time"] = finish_time
		elements["km_start_work"] = km_start_work
		elements["km_end_work"] = km_end_work
		elements["km_run_work"] = km_run_work
		elements["total_fuel_cost"] = total_fuel_cost
		elements["total_liter"] = total_liter
		elements["km_fill"] = km_fill
		elements["total_expressway_fees"] = total_expressway_fees
		elements["total_parking_fee"] = total_parking_fee
		elements["car_mirror"] = car_mirror
		elements["car_lights"] = car_lights
		elements["brake_light"] = brake_light
		elements["tire"] = tire
		elements["wiper"] = wiper
		elements["key_lock"] = key_lock
		elements["spare_tire"] = spare_tire
		elements["water_boiler"] = water_boiler
		elements["brake_pads"] = brake_pads
		elements["glass_washing_water"] = glass_washing_water
		elements["engine_oil"] = engine_oil
		elements["battery_pot_water"] = battery_pot_water
		elements["telephone"] = telephone
		elements["tr_note"] = tr_note
		elements["number_jobs_sent"] = number_jobs_sent
		elements["number_jobs_nosent"] = number_jobs_nosent
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Gettr_before_delivery_checklistbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("select * from tr_before_delivery_checklist where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var trbillnumber string
		var id_trbillnumber string
		var driver_code string
		var linetr string
		var carregistration string
		var date string
		var branch string
		var forklift_code string
		var start_time string
		var time_departure string
		var first_job_time string
		var final_job_time string
		var return_time string
		var finish_time string
		var km_start_work string
		var km_end_work string
		var km_run_work string
		var total_fuel_cost string
		var total_liter string
		var km_fill string
		var total_expressway_fees string
		var total_parking_fee string
		var car_mirror int
		var car_lights int
		var brake_light int
		var tire int
		var wiper int
		var key_lock int
		var spare_tire int
		var water_boiler int
		var brake_pads int
		var glass_washing_water int
		var engine_oil int
		var battery_pot_water int
		var telephone int
		var tr_note string
		var number_jobs_sent string
		var number_jobs_nosent string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(
			&id,
			&trbillnumber,
			&id_trbillnumber,
			&driver_code,
			&linetr,
			&carregistration,
			&date,
			&branch,
			&forklift_code,
			&start_time,
			&time_departure,
			&first_job_time,
			&final_job_time,
			&return_time,
			&finish_time,
			&km_start_work,
			&km_end_work,
			&km_run_work,
			&total_fuel_cost,
			&total_liter,
			&km_fill,
			&total_expressway_fees,
			&total_parking_fee,
			&car_mirror,
			&car_lights,
			&brake_light,
			&tire,
			&wiper,
			&key_lock,
			&spare_tire,
			&water_boiler,
			&brake_pads,
			&glass_washing_water,
			&engine_oil,
			&battery_pot_water,
			&telephone,
			&tr_note,
			&number_jobs_sent,
			&number_jobs_nosent,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["trbillnumber"] = trbillnumber
		elements["id_trbillnumber"] = id_trbillnumber
		elements["driver_code"] = driver_code
		elements["linetr"] = linetr
		elements["carregistration"] = carregistration
		elements["date"] = date
		elements["branch"] = branch
		elements["forklift_code"] = forklift_code
		elements["start_time"] = start_time
		elements["time_departure"] = time_departure
		elements["first_job_time"] = first_job_time
		elements["final_job_time"] = final_job_time
		elements["return_time"] = return_time
		elements["finish_time"] = finish_time
		elements["km_start_work"] = km_start_work
		elements["km_end_work"] = km_end_work
		elements["km_run_work"] = km_run_work
		elements["total_fuel_cost"] = total_fuel_cost
		elements["total_liter"] = total_liter
		elements["km_fill"] = km_fill
		elements["total_expressway_fees"] = total_expressway_fees
		elements["total_parking_fee"] = total_parking_fee
		elements["car_mirror"] = car_mirror
		elements["car_lights"] = car_lights
		elements["brake_light"] = brake_light
		elements["tire"] = tire
		elements["wiper"] = wiper
		elements["key_lock"] = key_lock
		elements["spare_tire"] = spare_tire
		elements["water_boiler"] = water_boiler
		elements["brake_pads"] = brake_pads
		elements["glass_washing_water"] = glass_washing_water
		elements["engine_oil"] = engine_oil
		elements["battery_pot_water"] = battery_pot_water
		elements["telephone"] = telephone
		elements["tr_note"] = tr_note
		elements["number_jobs_sent"] = number_jobs_sent
		elements["number_jobs_nosent"] = number_jobs_nosent
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Addtr_before_delivery_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	rows, err := DB.Exec("INSERT INTO tr_before_delivery_checklist (`trbillnumber`, `driver_code`, `linetr`, `carregistration`, `date`, `branch`, `forklift_code`, `start_time`, `time_departure`, `first_job_time`, `final_job_time`, `return_time`, `finish_time`, `km_start_work`, `km_end_work`, `km_run_work`, `total_fuel_cost`, `total_liter`, `km_fill`, `total_expressway_fees`, `total_parking_fee`, `car_mirror`, `car_lights`, `brake_light`, `tire`, `wiper`, `key_lock`, `spare_tire`, `water_boiler`, `brake_pads`, `glass_washing_water`, `engine_oil`, `battery_pot_water`, `telephone`, `tr_note`, `number_jobs_sent`, `number_jobs_nosent`, `create_date`, `create_at`, `modify_date`, `modify_at`)   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["trbillnumber"], json_map["driver_code"], json_map["linetr"], json_map["carregistration"], json_map["date"], json_map["branch"], json_map["forklift_code"], json_map["start_time"], json_map["time_departure"], json_map["first_job_time"], json_map["final_job_time"], json_map["return_time"], json_map["finish_time"], json_map["km_start_work"], json_map["km_end_work"], json_map["km_run_work"], json_map["total_fuel_cost"], json_map["total_liter"], json_map["km_fill"], json_map["total_expressway_fees"], json_map["total_parking_fee"], json_map["car_mirror"], json_map["car_lights"], json_map["brake_light"], json_map["tire"], json_map["wiper"], json_map["key_lock"], json_map["spare_tire"], json_map["water_boiler"], json_map["brake_pads"], json_map["glass_washing_water"], json_map["engine_oil"], json_map["battery_pot_water"], json_map["telephone"], json_map["tr_note"], json_map["number_jobs_sent"], json_map["number_jobs_nosent"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])

	if err != nil {
		panic(err)
	}
	lastID, err := rows.LastInsertId()
	println("The last inserted row id:", lastID)
	if err != nil {
		panic(err)
	}

	res := make(map[string]interface{})

	res["data"] = lastID
	return res["data"]
}
func Updatetr_before_delivery_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	var stringupdate string

	if json_map["trbillnumber"] != nil {
		stringupdate += "trbillnumber = '" + json_map["trbillnumber"].(string) + "',"
	}
	if json_map["driver_code"] != nil {
		stringupdate += "driver_code = '" + json_map["driver_code"].(string) + "',"
	}
	if json_map["linetr"] != nil {
		stringupdate += "linetr = '" + json_map["linetr"].(string) + "',"
	}
	if json_map["carregistration"] != nil {
		stringupdate += "carregistration = '" + json_map["carregistration"].(string) + "',"
	}
	if json_map["date"] != nil {
		stringupdate += "date = '" + json_map["date"].(string) + "',"
	}
	if json_map["branch"] != nil {
		stringupdate += "branch = '" + json_map["branch"].(string) + "',"
	}
	if json_map["forklift_code"] != nil {
		stringupdate += "forklift_code = '" + json_map["forklift_code"].(string) + "',"
	}
	if json_map["start_time"] != nil {
		stringupdate += "start_time = '" + json_map["start_time"].(string) + "',"
	}
	if json_map["time_departure"] != nil {
		stringupdate += "time_departure = '" + json_map["time_departure"].(string) + "',"
	}
	if json_map["first_job_time"] != nil {
		stringupdate += "first_job_time = '" + json_map["first_job_time"].(string) + "',"
	}
	if json_map["final_job_time"] != nil {
		stringupdate += "final_job_time = '" + json_map["final_job_time"].(string) + "',"
	}
	if json_map["return_time"] != nil {
		stringupdate += "return_time = '" + json_map["return_time"].(string) + "',"
	}
	if json_map["finish_time"] != nil {
		stringupdate += "finish_time = '" + json_map["finish_time"].(string) + "',"
	}
	if json_map["km_start_work"] != nil {
		stringupdate += "km_start_work = '" + json_map["km_start_work"].(string) + "',"
	}
	if json_map["km_end_work"] != nil {
		stringupdate += "km_end_work = '" + json_map["km_end_work"].(string) + "',"
	}
	if json_map["km_run_work"] != nil {
		stringupdate += "km_run_work = '" + json_map["km_run_work"].(string) + "',"
	}
	if json_map["total_fuel_cost"] != nil {
		stringupdate += "total_fuel_cost = '" + json_map["total_fuel_cost"].(string) + "',"
	}
	if json_map["total_liter"] != nil {
		stringupdate += "total_liter = '" + json_map["total_liter"].(string) + "',"
	}
	if json_map["km_fill"] != nil {
		stringupdate += "km_fill = '" + json_map["km_fill"].(string) + "',"
	}
	if json_map["total_expressway_fees"] != nil {
		stringupdate += "total_expressway_fees = '" + json_map["total_expressway_fees"].(string) + "',"
	}
	if json_map["total_parking_fee"] != nil {
		stringupdate += "total_parking_fee = '" + json_map["total_parking_fee"].(string) + "',"
	}

	if json_map["tr_note"] != nil {
		stringupdate += "tr_note = '" + json_map["tr_note"].(string) + "',"
	}
	if json_map["number_jobs_sent"] != nil {
		stringupdate += "number_jobs_sent = '" + json_map["number_jobs_sent"].(string) + "',"
	}
	if json_map["number_jobs_nosent"] != nil {
		stringupdate += "number_jobs_nosent = '" + json_map["number_jobs_nosent"].(string) + "',"
	}
	if json_map["create_date"] != nil {
		stringupdate += "create_date = '" + json_map["create_date"].(string) + "',"
	}
	if json_map["create_at"] != nil {
		stringupdate += "create_at = '" + json_map["create_at"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}
	if json_map["car_mirror"] != nil {
		str := strconv.FormatFloat(json_map["car_mirror"].(float64), 'E', -1, 64)
		stringupdate += "car_mirror = " + str + ","
	}
	if json_map["car_lights"] != nil {
		str := strconv.FormatFloat(json_map["car_lights"].(float64), 'E', -1, 64)
		stringupdate += "car_lights =" + str + ","
	}
	if json_map["brake_light"] != nil {
		str := strconv.FormatFloat(json_map["brake_light"].(float64), 'E', -1, 64)
		stringupdate += "brake_light = " + str + ","
	}
	if json_map["tire"] != nil {
		str := strconv.FormatFloat(json_map["brake_light"].(float64), 'E', -1, 64)
		stringupdate += "tire =  " + str + ","
	}
	if json_map["wiper"] != nil {
		str := strconv.FormatFloat(json_map["brake_light"].(float64), 'E', -1, 64)
		stringupdate += "wiper =  " + str + ","
	}
	if json_map["key_lock"] != nil {
		str := strconv.FormatFloat(json_map["brake_light"].(float64), 'E', -1, 64)
		stringupdate += "key_lock =  " + str + ","
	}
	if json_map["spare_tire"] != nil {
		str := strconv.FormatFloat(json_map["spare_tire"].(float64), 'E', -1, 64)
		stringupdate += "spare_tire =  " + str + ","
	}
	if json_map["water_boiler"] != nil {
		str := strconv.FormatFloat(json_map["water_boiler"].(float64), 'E', -1, 64)
		stringupdate += "water_boiler = " + str + ","
	}
	if json_map["brake_pads"] != nil {
		str := strconv.FormatFloat(json_map["brake_pads"].(float64), 'E', -1, 64)
		stringupdate += "brake_pads =  " + str + ","
	}
	if json_map["glass_washing_water"] != nil {
		str := strconv.FormatFloat(json_map["glass_washing_water"].(float64), 'E', -1, 64)
		stringupdate += "glass_washing_water =  " + str + ","
	}
	if json_map["engine_oil"] != nil {
		str := strconv.FormatFloat(json_map["engine_oil"].(float64), 'E', -1, 64)
		stringupdate += "engine_oil =  " + str + ","
	}
	if json_map["battery_pot_water"] != nil {
		str := strconv.FormatFloat(json_map["battery_pot_water"].(float64), 'E', -1, 64)
		stringupdate += "battery_pot_water =  " + str + ","
	}
	if json_map["telephone"] != nil {
		str := strconv.FormatFloat(json_map["telephone"].(float64), 'E', -1, 64)
		stringupdate += "telephone =  " + str + ","
	}
	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE tr_before_delivery_checklist
	SET ` + stringupdate + `
	
	WHERE id = ?`
	rows, err := DB.Exec(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	if err != nil {
		panic(err)
	}
	count, err := rows.RowsAffected()
	if err != nil {
		panic(err)
	}

	res := make(map[string]interface{})
	res["data"] = count
	return res["data"]
}
func Getsa_work_sa_ok(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("SELECT sa_n.invoice_number, sa_n.invoice_price FROM sa_invoice_number_checklist sa_n INNER JOIN sa_invoice_checklist sa_i ON sa_n.id = sa_i.id_invoice_list ")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var invoice_number string
		var invoice_price string

		rows.Scan(
			&invoice_number,
			&invoice_price,
		)
		elements := make(map[string]interface{})
		elements["invoice_number"] = invoice_number
		elements["invoice_price"] = invoice_price
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func trimLastChar(s string) string {
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}
	return s[:len(s)-size]
}
